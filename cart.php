<?php
    session_start();
    include('server.php');

    if(!isset($_SESSION['username'])){
        $_SESSION['msg'] = "คุณต้องเข้าสู่ระบบก่อน!";
        header('location: login.php');
    }
    if (isset($_GET['logout'])) {
        session_destroy();
        unset($_SESSION['username']);
        header('location: login.php');
    }
        $username = $_SESSION['username'];
        $mem = "SELECT `mem_id` FROM `member` WHERE `username` = '$username'";
        $result = mysqli_query($conn, $mem);
        $getdata = mysqli_fetch_array($result);
        $mem_id = $getdata['mem_id'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <title>BOOK4U</title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" 
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <style type="text/css">
    @font-face{
        font-family: Arabica;
        src: url(font/Arabica) format("truetype");
    }
    h{
        color:white; font-size:140%; margin-left:12%;
    }
    body{
        font-family: 'Arabica';
        background-color: #F3F3F7;
    }
    hr {
        display: block;
        height: 1px;
        border-top: 1px solid #ccc;
        margin: 1em 0;
        margin-right:4%;
    }
    </style>
</head>
<body>
    <div class="w3-sidebar w3-bar-block" style="background-color: #05A790; width:15%">
        <img src="image/Logo.jpg" width="160" height="160" style ="margin-left:13%; margin-top: 8%">
        <a href="home.php" class="w3-bar-item w3-button"
            style="margin-top:15%; text-decoration: none"><h>ร้านหนังสือ</h></a><br>
        <a href="popular.php" class="w3-bar-item w3-button" style="text-decoration: none"><h>ยอดนิยม</h></a><br>
        <a href="new.php" class="w3-bar-item w3-button" style="text-decoration: none"><h>มาใหม่</h></a><br>
        <a href="history.php" class="w3-bar-item w3-button" style="text-decoration: none"><h>ประวัติการสั่งซื้อ</h></a>
    </div>
    <div style="font-size:150%; margin-left:86%; color: gray">
        <!-- login user information -->
        <?php if (isset($_SESSION['username'])) : ?>
        <a href="cart.php"><i class="fas fa-shopping-basket" style="margin-right:5%; font-size:25px"></i></a>
        <a href="profile.php" style="font-size:30px; margin-right:5%"><?php echo $_SESSION['username'];?></a>|
        <a href="home.php?logout='1'"><i class="fas fa-sign-out-alt" style="margin-left:5%; font-size:25px"></i></a>
        <?php endif ?>
    </div>
    <form action="search.php" method="get" class="form-inline" style="margin-left:20%">
        <input class="form-control mr-sm-2" type="search" name="txtKeyword"
                size="35" placeholder="หนังสือ/ผู้แต่ง/สำนักพิมพ์" aria-label="Search">
        <button class="w3-button w3-white w3-border w3-round-large w3-padding-small" type="submit" name="search">ค้นหา</button>
    </form>
    <br>
    <h2 style="font-family:'Arabica'; margin-left:20%">ตะกร้าของคุณ</h2>
    <hr style="margin-left:20%"/>
    <br>
    <div style="margin-left:20%">
        <?php
        $query = "SELECT book.*,cart.* FROM book INNER JOIN cart ON book.isbn = cart.isbn AND cart.mem_id = '$mem_id' ORDER BY cart.isbn ASC" or die("isbn:" . mysqli_isbn()); 
        $result = mysqli_query($conn, $query);
        $totalprice = 0;
        if($result != NULL) {?>
        <?php while($row = mysqli_fetch_array($result)) {?>
            <div class="card mb-3" style="max-width: 540px;">
                <div class="row no-gutters">
                    <div class="col-md-4">
                    <img src="image/book/<?php echo $row['coverpic'];?>" width="120" height="150">
                    </div>
                    <div class="col-md-8">
                    <div class="card-body">
                        <h4 class="card-title" style="font-family:'Arabica'; color:gray"><?php echo $row['book_name'];?></h4>
                        <p class="card-text" style="font-size:20px; color:gray">฿ <?php echo $row['price'];?></p>
                        <?php $totalprice += $row['price'];?>
                        <form action="cart_db.php?isbn=<?php echo $row['isbn'];?>&act=remove" method="post">
                            <button class="w3-button w3-red w3-round-large w3-padding-small" type="submit"
                                    style="font-size:16px; margin-top:1%; margin-bottom:3%">ลบรายการ</button>
                        </form>
                    </div>
                    </div>
                </div>
            </div>
            <hr/>
        <?php 
            }
        }?>
    </div>
    <div style="margin-left:84%">
    <h3 style="font-family:'Arabica'">รวมทั้งหมด <?php echo $totalprice;?> บาท</h3>
    <div class="buy">
        <form action="paid.php?act=paid" method="post">
            <button class="w3-button w3-red w3-padding-small" type="submit"
                    style="font-size:20px; margin-bottom:40%; width: 140px; margin-left:6%">ชำระเงิน</button>
        </form>
    </div>
    </div>
</body>
</html>