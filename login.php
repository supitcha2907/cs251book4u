<?php 
    session_start();
    include('server.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <title>BOOK4U</title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" 
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    <style type="text/css">
    @font-face{
        font-family: Arabica;
        src: url(font/Arabica) format("truetype");
    }
    body{
        font-family: 'Arabica';
        background-color: #F3F3F7;
    }
    h{
        color:white; font-size:70%; font-family: 'Arabica';
    }
    .icon {
        padding: 10px;
        background: #05A790;
        color: white;
        min-width: 50px;
        text-align: center;
        font-size: 20px;
    }
    .input-field {
        width: 70%;
        padding: 5px;
        outline: none;
        font-size: 110%;
    }
    .button1 {
        background-color: #05A790;
        border: none;
        color: white;
        padding: 8px 32px;
        text-align: center;
        text-decoration: none;
        cursor: pointer;
    }
    .button2 {border-radius: 12px;}
    .part{
        float: left;
        width: 50%; 
    }
    </style>
</head>
<body>
    <div class="w3-container " style="background-color: #05A790">
            <img src="image/Logo.jpg" width="130" height="130" style ="margin-left:3%">
            <h><a href="register.php" class="button" style ="margin-left:78%; font-size: 200%; text-decoration: none;">
            <i class="fas fa-address-card" style ="color: white; font-size:25px"></i> สมัครสมาชิก</a></h>
    </div>
    <div class="row">
        <div class="part">
            <br><br><br><br>
            <div class="w3-card-4" style="background-color: #FFFFFF; width:75%;
                                        margin-left:20%; padding: 50px; border-radius: 8px">
                <div class="w3-container" style="background-color: #FFFFFF">
                    <h1 style="font-family:'Arabica'; text-align: center">เข้าสู่ระบบ</h1>
                </div>
                <form action="login_db.php" method="post">
                    <div style="text-align: center">
                        <div class="input-container"><i class="fa fa-user icon"></i>
                            <input class="input-field" type="text" placeholder="ชื่อผู้ใช้" name="username">
                        </div>
                        <br>
                        <div class="input-container"><i class="fa fa-lock icon"></i>
                            <input class="input-field" type="password" placeholder="รหัสผ่าน" name="password">
                        </div>
                        <?php include('errors.php');?>

                        <?php if (isset($_SESSION['error'])) :?>
                            <div class="error">
                                <br>
                                <h4 style="font-family:'Arabica'; color: red; text-align: center; line-height: 0.3">
                                    <?php
                                        echo $_SESSION['error'];
                                        unset($_SESSION['error']);
                                    ?>
                                </h4>
                            </div>
                        <?php endif ?>

                        <?php if (isset($_SESSION['msg'])) :?>
                            <div class="msg">
                            <br>
                                <h4 style="font-family:'Arabica'; color: red; text-align: center; line-height: 0.3">
                                    <?php
                                        echo $_SESSION['msg'];
                                        unset($_SESSION['msg']);
                                    ?>
                                </h4>
                            </div>
                        <?php endif ?>
                        <br>
                        <button type="submit" class="btn btn-info button1 button2"name="login_user"
                                style="font-size: 120%">เข้าสู่ระบบ</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="part">
            <img src="image/พื้นหลังโปร่ง.png" width="580" height="580" style="margin-left:10%">
        </div>
    </div>
</body>
</html>