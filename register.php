<?php 
    session_start();
    include('server.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <title>BOOK4U</title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" 
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <style type="text/css">
    @font-face{
        font-family: Arabica;
        src: url(font/Arabica) format("truetype");
    }
    body{
        font-family: 'Arabica';
        background-color: #F3F3F7;
    }
    h{
        color:white; font-size:70%; font-family: 'Arabica';
    }
    .button1 {
        background-color: #05A790;
        border: none;
        color: white;
        padding: 8px 32px;
        text-align: center;
        text-decoration: none;
        cursor: pointer;
    }
    .button2 {border-radius: 12px;}
    .part{
        float: left;
        width: 50%; 
    }
    .w3-input {
        width: 85%;
        font-size: 110%;
        margin-left: 7%;
    }
    </style>
</head>
<body>
    <div class="w3-container " style="background-color: #05A790">
            <img src="image/Logo.jpg" width="130" height="130" style ="margin-left:3%">
            <h><a href="login.php" class="button" style ="margin-left:78%; font-size: 200%; text-decoration: none">
            <i class="fas fa-user" style ="color: white; font-size:25px"></i> เข้าสู่ระบบ</a></h>
    </div>
    <div class="row">
        <div class="part">
            <br><br>
            <div class="w3-card-4" style="background-color: #FFFFFF; width:65%;
                                        margin-left:25%; padding: 20px; border-radius: 8px">
                <div class="w3-container" style="background-color: #FFFFFF">
                    <h1 style="font-family:'Arabica'; text-align: center">สมัครสมาชิก</h1>
                </div>
                <form action="register_db.php" method="post">
                    <div style="text-align: center">
                        <?php include('errors.php');?>

                        <?php if (isset($_SESSION['error'])) :?>
                            <div class="error">
                                <h4 style="font-family:'Arabica'; color: red; text-align: center">
                                    <?php
                                        echo $_SESSION['error'];
                                        unset($_SESSION['error']);
                                    ?>
                                </h4>
                            </div>
                        <?php endif ?>
                        <?php if (isset($_SESSION['err'])) :?>
                            <div class="err">
                            <h4 style="font-family:'Arabica'; color: red; text-align: center">
                                    <?php
                                        echo $_SESSION['err'];
                                        unset($_SESSION['err']);
                                    ?>
                                </h4>
                            </div>
                        <?php endif ?>
                        <?php if (isset($_SESSION['success'])) :?>
                            <div class="success">
                                <script type="text/javascript">
                                    swal("คุณสมัครสมาชิกเรียบร้อยแล้ว", "", "success");
                                </script>
                                <?php
                                    unset($_SESSION['success']);
                                ?>
                            </div>
                        <?php endif ?>
                        <form class="w3-container ">
                            <input class="w3-input" type="text" name="fname" placeholder="ชื่อจริง * " required></p>
                            <input class="w3-input" type="text" name="lname" placeholder="นามสกุล * " required></p>
                            <input class="w3-input" type="text" name="username" placeholder="ชื่อผู้ใช้ * " required></p>
                            <input class="w3-input" type="password" name="password_1" placeholder="รหัสผ่าน * " required></p>
                            <input class="w3-input" type="password" name="password_2" placeholder="ยืนยันรหัสผ่าน * " required></p>
                            <input class="w3-input" type="email" name="email" placeholder="อีเมลล์ * " required></p>
                        </form>
                        <button type="submit" class="btn btn-info button1 button2"name="reg_user"
                                style="font-size: 120%">สมัครสมาชิก</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="part">
            <img src="image/พื้นหลังโปร่ง.png" width="580" height="580" style="margin-left:10%">
        </div>
    </div>
</body>
</html>