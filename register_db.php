<?php
    session_start();
    include('server.php');
    
    $error = array();

    if(isset($_POST['reg_user'])){
        $fname = mysqli_real_escape_string($conn, $_POST['fname']);
        $lname = mysqli_real_escape_string($conn, $_POST['lname']);
        $username = mysqli_real_escape_string($conn, $_POST['username']);
        $password_1 = mysqli_real_escape_string($conn, $_POST['password_1']);
        $password_2 = mysqli_real_escape_string($conn, $_POST['password_2']);
        $email = mysqli_real_escape_string($conn, $_POST['email']);

        if ($password_1 != $password_2) {
            array_push($error, "รหัสผ่านไม่ตรงกัน");
            $_SESSION['err'] = "รหัสผ่านไม่ตรงกัน";
            header("location: register.php");
        }

        $user_check_query = "SELECT * FROM member WHERE username = '$username' OR email = '$email'";
        $query = mysqli_query($conn, $user_check_query);
        $result = mysqli_fetch_assoc($query);

        if($result){
            if($result['username'] === $username){
                array_push($error, "มีชื่อผู้ใช้นี้แล้ว กรุณากรอกใหม่อีกครั้");
                $_SESSION['error'] = "มีชื่อผู้ใช้นี้แล้ว กรุณากรอกใหม่อีกครั้ง";
                header("location: register.php");
            }
            if($result['email'] === $email){
                array_push($error, "มีการใช้อีเมลล์นี้แล้ว กรุณากรอกใหม่อีกครั้");
                $_SESSION['error'] = "มีการใช้อีเมลล์นี้แล้ว กรุณากรอกใหม่อีกครั้ง";
                header("location: register.php");
            }
        }
        if (count($error) == 0) {
            $password = md5($password_1);

            $sql = "INSERT INTO member (fname, lname, email, username, passw) VALUES ('$fname', '$lname', '$email', '$username', '$password')";
            mysqli_query($conn, $sql);

            $_SESSION['success'] = "";
            header('location: register.php');
        }
    }
?>