<?php 
    session_start();
    include('server.php')
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <title>BOOK4U</title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" 
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <style type="text/css">
    @font-face{
        font-family: Arabica;
        src: url(font/Arabica) format("truetype");
    }
    h{
        color:white; font-size:140%; margin-left:12%;
    }
    body{
        font-family: 'Arabica';
        background-color: #F3F3F7;
    }
    hr {
        display: block;
        height: 1px;
        border-top: 1px solid #ccc;
        margin: 1em 0;
        margin-left:35%; margin-right:26%;
    }
    .button1 {
        background-color: #05A790;
        border: none;
        color: white;
        padding: 5px 28px;
        text-align: center;
        text-decoration: none;
        cursor: pointer;
    }
    .button2 {border-radius: 20px;}
    cc{
        color:gray;
    }
    </style>
</head>
<body>
    <div class="w3-sidebar w3-bar-block" style="background-color: #05A790; width:15%">
        <img src="image/Logo.jpg" width="160" height="160" style ="margin-left:13%; margin-top: 8%">
        <a href="home.php" class="w3-bar-item w3-button" style="margin-top:15%; text-decoration: none"><h>ร้านหนังสือ</h></a><br>
        <a href="popular.php" class="w3-bar-item w3-button" style="text-decoration: none"><h>ยอดนิยม</h></a><br>
        <a href="new.php" class="w3-bar-item w3-button" style="text-decoration: none"><h>มาใหม่</h></a><br>
        <a href="history.php" class="w3-bar-item w3-button" style="text-decoration: none"><h>ประวัติการสั่งซื้อ</h></a>
    </div>
    <div style="font-size:150%; margin-left:86%; color: gray">
        <!-- login user information -->
        <?php if (isset($_SESSION['username'])) : ?>
        <a href="cart.php"><i class="fas fa-shopping-basket" style="margin-right:5%; font-size:25px"></i></a>
        <a href="profile.php" style="font-size:30px; margin-right:5%"><?php echo $_SESSION['username'];?></a>|
        <a href="home.php?logout='1'"><i class="fas fa-sign-out-alt" style="margin-left:5%; font-size:25px"></i></a>
        <?php endif ?>
    </div>
    <br><br><br>
    <?php
        $username =  $_SESSION['username'];
        $query = "SELECT * FROM `member` WHERE `username`= '$username'";
        $result = mysqli_query($conn, $query);
        $row = mysqli_fetch_array($result);
    ?>
        <?php if ($row['pic']==null) :?>
            <i class="fas fa-user-circle" style="margin-left:51%; font-size:100px; color:gray"></i>
        <?php endif ?>
        <?php if ($row['pic']!=null) :?>
            <img src="image/user_pic/<?php echo $row['pic'];?>" width="100" height="100"
                style="margin-left:51%; font-size:100px; border-radius: 50%">
        <?php endif ?>
        <hr/>
        <form action="edit.php" method="post" style="font-size:22px">
                <p style="word-spacing:70px; margin-left:39%">ID <cc><?php echo $row['mem_id']; ?></cc></p>
                <p style="word-spacing:63px; margin-left:39%">ชื่อ <cc><?php echo $row['fname']; ?></cc> นามสกุล <cc><?php echo $row['lname']; ?></cc></p>
                <p style="word-spacing:36px; margin-left:39%">ชื่อผู้ใช้ <cc><?php echo $row['username']; ?></p></cc>
                <p style="word-spacing:36px; margin-left:39%">อีเมลล์ <cc><?php echo $row['email']; ?></cc></p>
                <hr/>
                <button type="submit" class="btn btn-info button1 button2"name="edit"
                        style="font-size:20px; margin-left:49%">แก้ไขข้อมูลส่วนตัว
                </button>
        </form>
</body>
</html>