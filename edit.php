<?php include('server.php')?>
<?php
    session_start();
    /*if(!isset($_SESSION['username'])){
        $_SESSION['msg'] = "คุณต้องเข้าสู่ระบบก่อน!";
        header('location: login.php');
    }
    if (isset($_GET['logout'])) {
        session_destroy();
        unset($_SESSION['username']);
        header('location: login.php');
    }*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <title>BOOK4U</title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" 
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <style type="text/css">
    @font-face{
        font-family: Arabica;
        src: url(font/Arabica) format("truetype");
    }
    h{
        color:white; font-size:140%; margin-left:12%;
    }
    body{
        font-family: 'Arabica';
        background-color: #F3F3F7;
    }
    hr {
        display: block;
        height: 1px;
        border-top: 1px solid #ccc;
        margin: 1em 0;
        margin-left:35%; margin-right:26%;
    }
    .button1 {
        background-color: #05A790;
        border: none;
        color: white;
        padding: 5px 28px;
        text-align: center;
        text-decoration: none;
        cursor: pointer;
    }
    .button2 {border-radius: 20px;}
    cc{
        color:gray;
    }
    .form-control {
        font-size: 18px;
        color:gray;
    }
    </style>
</head>
<body>
    <div class="w3-sidebar w3-bar-block" style="background-color: #05A790; width:15%">
        <img src="image/Logo.jpg" width="160" height="160" style ="margin-left:13%; margin-top: 8%">
        <a href="home.php" class="w3-bar-item w3-button" style="margin-top:15%; text-decoration: none"><h>ร้านหนังสือ</h></a><br>
        <a href="popular.php" class="w3-bar-item w3-button" style="text-decoration: none"><h>ยอดนิยม</h></a><br>
        <a href="new.php" class="w3-bar-item w3-button" style="text-decoration: none"><h>มาใหม่</h></a><br>
        <a href="history.php" class="w3-bar-item w3-button" style="text-decoration: none"><h>ประวัติการสั่งซื้อ</h></a>
    </div>
    <div style="font-size:150%; margin-left:86%; color: gray">
        <!-- login user information -->
        <?php if (isset($_SESSION['username'])) : ?>
        <a href="cart.php"><i class="fas fa-shopping-basket" style="margin-right:5%; font-size:25px"></i></a>
        <a href="profile.php" style="font-size:30px; margin-right:5%"><?php echo $_SESSION['username'];?></a>|
        <a href="home.php?logout='1'"><i class="fas fa-sign-out-alt" style="margin-left:5%; font-size:25px"></i></a>
        <?php endif ?>
    </div>
    <br><br><br>
    <?php
        $username =  $_SESSION['username'];
        $query = "SELECT * FROM `member` WHERE `username`= '$username'";
        $result = mysqli_query($conn, $query);
        $row = mysqli_fetch_assoc($result);
        $_SESSION['id'] = $row['mem_id'];
    ?>
    <form action="do_edit.php" method="post" enctype="multipart/form-data" id="form1">
        <?php if ($row['pic']==null) :?>
            <i class="fas fa-user-circle" style="margin-left:51%; font-size:100px; color:gray"></i>
        <?php endif ?>
        <?php if ($row['pic']!=null) :?>
            <img src="image/user_pic/<?php echo $row['pic'];?>" width="100" height="100"
                style="margin-left:51%; font-size:100px; border-radius: 50%">
        <?php endif ?>
        <br>
        <div class="pic">
        <label style="font-size:20px; margin-left:51%; color:darkblue">อัพโหลดรูปภาพ</label>
        <input type="file" name="pic" style="font-size:15px; margin-left:50%; color:gray">
        </div>
        <hr/>
        <form class="w3-container">
            <p style="word-spacing:70px; margin-left:39%; font-size:22px" name="id">ID <cc><?php echo $row['mem_id']; ?></cc>
            <div class="row"style="margin-left:38.1%">
                <div class="col-sm-2 col-form-label" >
                    <input type="text" class="form-control" style="width:170%"
                        placeholder="ชื่อจริง" name="fname" value="<?php echo $row['fname']; ?>" required>
                </div>
                <div class="col-sm-2 col-form-label">
                    <input type="text" class="form-control" style="width:170%; margin-left:62.2%" 
                        placeholder="นามสกุล" name="lname" value="<?php echo $row['lname']; ?>" required>
                </div>
            </div>
            <div class="form-group" style="margin-left:39%; margin-top:1%">
                <input type="text" class="form-control" style="width:50%"
                    placeholder="ชื่อผู้ใช้" value="<?php echo $row['username']; ?>" required>
            </div>
            <div class="form-group" style="margin-left:39%">
                <input type="email" class="form-control" style="width:50%"
                    placeholder="อีเมลล์" name="email" value="<?php echo $row['email']; ?>" required>
            </div>
            <hr/>
            <button type="submit" class="btn btn-info button1 button2"name="edit_user"
                style="font-size:20px; margin-left:49%">ยืนยันการแก้ไข
            </button>
        </form> 
    </form>
</body>
</html>