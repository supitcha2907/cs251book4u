<?php include('server.php')?>
<?php
    session_start();

    if(!isset($_SESSION['username'])){
        $_SESSION['msg'] = "คุณต้องเข้าสู่ระบบก่อน!";
        header('location: login.php');
    }
    if (isset($_GET['logout'])) {
        session_destroy();
        unset($_SESSION['username']);
        header('location: login.php');
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <title>BOOK4U</title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" 
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <style type="text/css">
    @font-face{
        font-family: Arabica;
        src: url(font/Arabica) format("truetype");
    }
    h{
        color:white; font-size:140%; margin-left:12%;
    }
    body{
        font-family: 'Arabica';
        background-color: #F3F3F7;
    }
    .cl {
        column-count: 5;
        column-gap: 25px;
    }
    </style>
</head>
<body>
    <div class="w3-sidebar w3-bar-block" style="background-color: #05A790; width:15%">
        <img src="image/Logo.jpg" width="160" height="160" style ="margin-left:13%; margin-top: 8%">
        <a href="home.php" class="w3-bar-item w3-button"
            style="margin-top:15%; text-decoration: none; background-color: #40BEB2"><h>ร้านหนังสือ</h></a><br>
        <a href="popular.php" class="w3-bar-item w3-button" style="text-decoration: none"><h>ยอดนิยม</h></a><br>
        <a href="new.php" class="w3-bar-item w3-button" style="text-decoration: none"><h>มาใหม่</h></a><br>
        <a href="history.php" class="w3-bar-item w3-button" style="text-decoration: none"><h>ประวัติการสั่งซื้อ</h></a>
    </div>
    <div style="font-size:150%; margin-left:86%; color: gray">
        <!-- login user information -->
        <?php if (isset($_SESSION['username'])) : ?>
        <a href="cart.php"><i class="fas fa-shopping-basket" style="margin-right:5%; font-size:25px"></i></a>
        <a href="profile.php" style="font-size:30px; margin-right:5%"><?php echo $_SESSION['username'];?></a>|
        <a href="home.php?logout='1'"><i class="fas fa-sign-out-alt" style="margin-left:5%; font-size:25px"></i></a>
        <?php endif ?>
    </div>
    <form action="search.php" method="get" class="form-inline" style="margin-left:20%">
        <input class="form-control mr-sm-2" type="search" name="txtKeyword"
                size="35" placeholder="หนังสือ/ผู้แต่ง/สำนักพิมพ์" aria-label="Search">
        <button class="w3-button w3-white w3-border w3-round-large w3-padding-small" type="submit" name="search">ค้นหา</button>
    </form>
    <br>
    <ul class="nav nav-tabs" style="margin-left:20%; margin-right:4%; font-size:18px">
        <li><a data-toggle="tab" href="home.php">การ์ตูน</a></li>
        <li><a data-toggle="tab" href="novel.php">นิยาย</a></li>
        <li><a data-toggle="tab" href="psycho.php">จิตวิทยา</a></li>
        <li><a data-toggle="tab" href="travel.php">ท่องเที่ยว</a></li>
        <li class="active"><a data-toggle="tab" href="business.php" style="color:#40BEB2">บริหารธุรกิจ</a></li>
        <li><a data-toggle="tab" href="kids.php">หนังสือเด็ก</a></li>
        <li class="nav-item">
    </ul>
    <br>
    <div class="cl"style="margin-left:20%;">
    <?php
    $query = "SELECT * FROM `book` WHERE `catagory_id` = '5'" or die("Error:" . mysqli_error()); 
    $result = mysqli_query($conn, $query);
    ?>
    <?php while($row = mysqli_fetch_array($result)){?>
        <a href="detail.php?isbn=<?php echo $row['isbn'];?>&act=detail">
        <div class="card" style="border-radius: 12px; width:11rem; text-align: center; background-color: #FFFFFF">
            <img src="image/book/<?php echo $row['coverpic'];?>" width="120" height="160" style="margin-top:7%"></a>
            <div class="card-body" style="font-size:110%">
                    <p class="card-text"style="margin-top:5%; margin-left:2%; margin-right:2%"><?php echo $row['book_name'];?>
                    <br><span class="badge badge-pill badge-light"
                            style="font-size:90%; border-style: solid; border-color: #40BEB2; background-color: #FFFFFF;
                                    border-width: 1.5px; color:#40BEB2">฿ <?php echo $row['price'];?></span>
            </div>
        </div>
    <?php }?>
    </div>
</body>
</html>