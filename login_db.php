<?php
    session_start();
    include('server.php');

    $error = array();

    if (isset($_POST['login_user'])) {
        $username = mysqli_real_escape_string($conn, $_POST['username']);
        $password = mysqli_real_escape_string($conn, $_POST['password']);

        if(empty($username) || empty($password) ){
            array_push($error, "กรุณากรอกชื่อผู้ใช้/รหัสผ่าน");
            $_SESSION['error'] = "กรุณากรอกข้อมูลให้ครบถ้วน";
            header("location: login.php");
        }

        if (count($error) == 0) {
            $password = md5($password);
            $query = "SELECT * FROM member WHERE username = '$username' AND passw = '$password'";
            $result = mysqli_query($conn, $query);

            if (mysqli_num_rows($result) == 1) {
                $_SESSION['username'] = $username;
                header("location: home.php");
            } else {
                array_push($error, "ชื่อผู้ใช้หรือรหัสผ่านผิด");
                $_SESSION['error'] = "ชื่อผู้ใช้หรือรหัสผ่านผิด กรุณากรอกใหม่อีกครั้ง";
                header("location: login.php");
            }
        }
    }
?>