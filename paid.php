<?php include('server.php')?>
<?php
    session_start();

    if(!isset($_SESSION['username'])){
        $_SESSION['msg'] = "คุณต้องเข้าสู่ระบบก่อน!";
        header('location: login.php');
    }
    if (isset($_GET['logout'])) {
        session_destroy();
        unset($_SESSION['username']);
        header('location: login.php');
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <title>BOOK4U</title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" 
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <style type="text/css">
    @font-face{
        font-family: Arabica;
        src: url(font/Arabica) format("truetype");
    }
    h{
        color:white; font-size:140%; margin-left:12%;
    }
    body{
        font-family: 'Arabica';
        background-color: #F3F3F7;
    }
    hr {
        display: block;
        height: 1px;
        border-top: 1px solid #ccc;
        margin: 1em 0;
        margin-right:4%;
    }
    .w3-input {
        width: 65%;
        font-size: 130%;
        background-color: #F3F3F7;
    }
    </style>
</head>
<body>
    <div class="w3-sidebar w3-bar-block" style="background-color: #05A790; width:15%">
        <img src="image/Logo.jpg" width="160" height="160" style ="margin-left:13%; margin-top: 8%">
        <a href="home.php" class="w3-bar-item w3-button"
            style="margin-top:15%; text-decoration: none"><h>ร้านหนังสือ</h></a><br>
        <a href="popular.php" class="w3-bar-item w3-button" style="text-decoration: none"><h>ยอดนิยม</h></a><br>
        <a href="new.php" class="w3-bar-item w3-button" style="text-decoration: none"><h>มาใหม่</h></a><br>
        <a href="history.php" class="w3-bar-item w3-button" style="text-decoration: none"><h>ประวัติการสั่งซื้อ</h></a>
    </div>
    <div style="font-size:150%; margin-left:86%; color: gray">
        <!-- login user information -->
        <?php if (isset($_SESSION['username'])) : ?>
        <a href="cart.php"><i class="fas fa-shopping-basket" style="margin-right:5%; font-size:25px"></i></a>
        <a href="profile.php" style="font-size:30px; margin-right:5%"><?php echo $_SESSION['username'];?></a>|
        <a href="home.php?logout='1'"><i class="fas fa-sign-out-alt" style="margin-left:5%; font-size:25px"></i></a>
        <?php endif ?>
    </div>
    <br><br>
    <h2 style="font-family:'Arabica'; margin-left:20%">เพิ่มบัตรเครดิต/บัตรเดบิต</h2>
    <hr style="margin-left:20%"/>
    <form action="paid_db.php" method="post" style="margin-left:23%">
        <form class="w3-container" style="margin-left:20%">
            <input class="w3-input" type="text" name="name" placeholder="ชื่อที่ปรากฏบนบัตร" required></p>
            <input class="w3-input" type="text" name="number" placeholder="หมายเลขบัตร" required></p>
            <input class="w3-input" type="text" name="typecard" placeholder="ประเภทของบัตร" required></p>
            <input class="w3-input" type="text" name="exp" placeholder="วันหมดอายุ" required></p>
            <input class="w3-input" type="text" name="ccv" placeholder="CVV" required></p>
            <br>
            <button class="w3-button w3-red w3-padding-small" type="submit" name="paid" 
                    style="font-size:20px; width: 140px; margin-left:53%">ชำระเงิน
            </button>
        </form>
    </form>
</body>
</html>