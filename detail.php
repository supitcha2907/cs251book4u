<?php include('server.php')?>
<?php
    session_start();

    if(!isset($_SESSION['username'])){
        $_SESSION['msg'] = "คุณต้องเข้าสู่ระบบก่อน!";
        header('location: login.php');
    }
    if (isset($_GET['logout'])) {
        session_destroy();
        unset($_SESSION['username']);
        header('location: login.php');
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <title>BOOK4U</title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" 
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <style type="text/css">
    @font-face{
        font-family: Arabica;
        src: url(font/Arabica) format("truetype");
    }
    h{
        color:white; font-size:140%; margin-left:12%;
    }
    body{
        font-family: 'Arabica';
        background-color: #F3F3F7;
    }
    </style>
</head>
<body>
    <div class="w3-sidebar w3-bar-block" style="background-color: #05A790; width:15%">
        <img src="image/Logo.jpg" width="160" height="160" style ="margin-left:13%; margin-top: 8%">
        <a href="home.php" class="w3-bar-item w3-button"
            style="margin-top:15%; text-decoration: none"><h>ร้านหนังสือ</h></a><br>
        <a href="popular.php" class="w3-bar-item w3-button" style="text-decoration: none"><h>ยอดนิยม</h></a><br>
        <a href="new.php" class="w3-bar-item w3-button" style="text-decoration: none"><h>มาใหม่</h></a><br>
        <a href="history.php" class="w3-bar-item w3-button" style="text-decoration: none"><h>ประวัติการสั่งซื้อ</h></a>
    </div>
    <div style="font-size:150%; margin-left:86%; color: gray">
        <!-- login user information -->
        <?php if (isset($_SESSION['username'])) : ?>
        <a href="cart.php"><i class="fas fa-shopping-basket" style="margin-right:5%; font-size:25px"></i></a>
        <a href="profile.php" style="font-size:30px; margin-right:5%"><?php echo $_SESSION['username'];?></a>|
        <a href="home.php?logout='1'"><i class="fas fa-sign-out-alt" style="margin-left:5%; font-size:25px"></i></a>
        <?php endif ?>
    </div>
    <form action="search.php" method="get" class="form-inline" style="margin-left:20%">
        <input class="form-control mr-sm-2" type="search" name="txtKeyword"
                size="35" placeholder="หนังสือ/ผู้แต่ง/สำนักพิมพ์" aria-label="Search">
        <button class="w3-button w3-white w3-border w3-round-large w3-padding-small" type="submit" name="search">ค้นหา</button>
    </form>
    <br><br><br>
    <div style="margin-left:25%;">
    <?php
    if (isset($_GET['isbn'])) {
        $isbn = $_GET['isbn'];
      }
    $username = $_SESSION['username'];
    
    $query = "SELECT * FROM author AS A1
    RIGHT JOIN book AS A2 ON A1.author_id = A2.author_id
    INNER JOIN publishing_company AS A3 ON A2.pub_id= A3.pub_id AND A2.isbn = '$isbn'" or die("Error:" . mysqli_error()); 
    $result = mysqli_query($conn, $query);
    $row = mysqli_fetch_array($result);
    ?>
    <div class="card mb-3" style="max-width: 1000px; background-color: #FFFFFF">
    <button type="button" class="close" style="margin-top:1%; margin-right:2%">
        <a onclick='window.history.back()' style="font-size:45px; text-decoration: none">
        <span aria-hidden="true">&times;</span></a>
    </button>
        <div class="row no-gutters">
            <div class="col-md-4 ">
            <img class="w3-card-4" src="image/book/<?php echo $row['coverpic'];?>" width="250" height="350"
                style="margin-left:10%; margin-bottom:10%; margin-top:10%">
            </div>
            <div class="col-md-8">
            <div class="card-body">
                <h1 style="font-family:'Arabica'"><?php echo $row['book_name'];?></h1>
                <p style="font-size: 20px">โดย <?php echo $row['author_name'];?></p>
                <p style="font-size: 20px">สำนักพิมพ์ : <?php echo $row['pub_name'];?></p>
                <p style="margin-right:7%; font-size:18px; color:gray"><?php echo $row['datail'];?></p>
                <p style="font-size: 23px">฿ <?php echo $row['price'];?></p>
                <form action="cart_db.php?isbn=<?php echo $row['isbn'];?>&act=add" method="post">
                <button class="w3-button w3-green w3-round-large w3-padding-small" type="submit"
                        style="font-size:20px; margin-bottom:3%">สั่งซื้อ</button>
                </form>
            </div>
            </div>
        </div>
    </div>
</body>
</html>